Installation

1. Rename folder to your_theme.
2. Rename .info-file to your_theme.info.
3. Edit .info-file and fill in name and description.
4. Edit template.php-file and rename g_raph__breadcrumb to your_theme_breadcrumb.

Ready to go!
